package Vehiculos;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean repetir = true;
        
        while (repetir) {
            System.out.println("Ingrese la marca del vehículo:");
            String marca = scanner.nextLine();

            System.out.println("Ingrese el modelo del vehículo:");
            String modelo = scanner.nextLine();

            System.out.println("Ingrese el precio del vehículo:");
            double precio = scanner.nextDouble();

            System.out.println("Presione 1,2 ó 3 para escoger el tipo de vehículo (1-Camion, 2-Furgoneta, 3-Auto):");
            int tipoVehiculo = scanner.nextInt();
            scanner.nextLine(); 

            switch (tipoVehiculo) {
                case 1:
                    System.out.println("Ingrese la capacidad de carga del camión:");
                    double capacidadCarga = scanner.nextDouble();
                    Camion camion = new Camion(marca, modelo, precio, capacidadCarga);
                    System.out.println("Camión creado: " + camion.getMarca() + " | Modelo: " + camion.getModelo() + " | Precio: " + camion.getPrecio() + " |Capacidad de carga: " + camion.getCapacidadCarga());
                    break;
                case 2:
                    System.out.println("Ingrese el volumen de carga de la furgoneta:");
                    double volumenCarga = scanner.nextDouble();
                    Furgoneta furgoneta = new Furgoneta(marca, modelo, precio, volumenCarga);
                    System.out.println("Furgoneta creada: " + furgoneta.getMarca() + " | Modelo: " + furgoneta.getModelo() + " | Precio: " + furgoneta.getPrecio() + " | Volumed de carga " + furgoneta.getVolumenCarga());
                    break;
                case 3:
                    System.out.println("Ingrese la cantidad de puertas del auto:");
                    int cantidadPuertas = scanner.nextInt();
                    Auto auto = new Auto(marca, modelo, precio, cantidadPuertas);
                    System.out.println("Auto creado: " + auto.getMarca() + " | Modelo: " + auto.getModelo() + " | Precio: " + auto.getPrecio() + " | Cantidad de puertas: " + auto.getCantidadPuertas());
                    break;
                default:
                    System.out.println("Tipo de vehículo no válido.");
            }
                        System.out.println("¿Desea crear otro vehículo? (s/n)");
            String respuesta = scanner.nextLine();
            repetir = respuesta.equalsIgnoreCase("s");
        }
		
	}

}
